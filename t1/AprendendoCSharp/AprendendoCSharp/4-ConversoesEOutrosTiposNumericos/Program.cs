﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _4_ConversoesEOutrosTiposNumericos
{
    class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("Executando projeto 4 - Conversoes e outros tipos numericos");

            double salario = 1200.50;

            //Int é um tipo de variavel que suporta valores de 32 bits
            int salarioEmInteiro = (int) salario;
            Console.WriteLine(salarioEmInteiro);

            //O long é variavel de 64bits
            long idade = 13000000000;
            Console.WriteLine(idade);

            //O short suporta variavel de 16 bits
            short quantidadeProdutos = 15000;
            Console.WriteLine(idade);

            float altura = 1.80f;
            Console.WriteLine(altura);




            Console.WriteLine("Aplicação finalizou, tecle enter para finalizar...");
            Console.ReadLine();

        }
    }
}
