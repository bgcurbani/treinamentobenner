﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesafioMultiploDe3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Executando desafio multiplo de 3");
            for (int numero = 1; numero < 100; numero++)
            {
                if( numero % 3 == 0)
                {
                    Console.WriteLine(numero);
                }
            }

            Console.WriteLine();
            Console.WriteLine("Execução finalizada, tecle enter apara sair...");
            Console.ReadLine();
        }
    }
}
