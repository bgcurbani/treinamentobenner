var logger = require('../servicos/logger.js');
module.exports = (app) => {

    const PAGAMENTO_CRIADO = "CRIADO";
    const PAGAMENTO_CONFIRMADO = "CONFIRMADO";
    const PAGAMENTO_CANCELADO = "CANCELADO";

    app.get('/pagamentos', function(req, res) {
        console.log('recebida requisção de teste');
        res.send('OK');
    });

    app.get('/pagamentos/pagamento/:id', function(req, res){
        var id = req.params.id;
        console.log('consultando pagamento: '+ id);

        logger.info('consultando pagamento: '+ id);

        var memcachedClient = app.servicos.memcachedClient();

        memcachedClient.get('pagamento-'+id, function(erro, retorno){
            if(erro || !retorno) {
                console.log('MISS - chave não encontrada');

                var connection = app.persistencia.connectionFactory();
                var pagamentoDao = new app.persistencia.PagamentoDao(connection);

                pagamentoDao.buscaPorId(id, function(erro, resultado) {
                    if(erro) {
                        console.log('Erro ao consultar no banco: ' + erro);
                        res.status(500).send(erro);
                        return;
                    }

                    console.log('pagamento encontrado: '+ JSON.stringify(resultado));
                    res.json(resultado);
                });
            } else {
                console.log('HIT - valor: ' + JSON.stringify(retorno));
                res.json(retorno);
            }
        });

        
    });

    app.put('/pagamentos/pagamento/:id', function(req, res) {
        var id = req.params.id;
        var pagamento = {};
        pagamento.id = id;
        pagamento.status = PAGAMENTO_CONFIRMADO;


        var connection = app.persistencia.connectionFactory();
        var pagamentoDao = new app.persistencia.PagamentoDao(connection);

        pagamentoDao.atualiza(pagamento, function(erro) {
            if(erro){
                res.status(500).send(erro);
                return;
            }
            console.log('pagamento confirmado');
            res.send(pagamento);
        });

    });

    app.delete('/pagamentos/pagamento/:id', function(req, res) {

        var id = req.params.id;
        var pagamento = {};

        pagamento.id = id;
        pagamento.status = PAGAMENTO_CANCELADO;


        var connection = app.persistencia.connectionFactory();
        var pagamentoDao = new app.persistencia.PagamentoDao(connection);

        pagamentoDao.atualiza(pagamento, function(erro) {
            if(erro){
                res.status(500).send(erro);
                return;
            }
            console.log('pagamento cancelado');
            res.status(204).send(pagamento);
        });


    });

    app.post('/pagamentos/pagamento', function(req, res) {

        req.assert("pagamento.forma_de_pagamento", 
                   "Forma de pagamento é obrigatorio").notEmpty();
        req.assert("pagamento.valor", "Valor é obrigadorio e deve ser um decimal").notEmpty().isFloat();

        var erros = req.validationErrors();

        if(erros) {
            console.log('Erros de validacao encontrados');
            res.status(400).send(erros);
            return;
        }

        var pagamento = req.body["pagamento"];
        console.log(pagamento);
        
        pagamento.status = PAGAMENTO_CRIADO;
        pagamento.data = new Date();

        var connection = app.persistencia.connectionFactory();
        var pagamentoDao = new app.persistencia.PagamentoDao(connection);

        pagamentoDao.salva(pagamento, function(erro, resultado) {
            if(erro){
                console.log('Erro ao inserir no banco: ' + erro);
                req.status(400).send(erro);
            } else {
                pagamento.id = resultado.insertId;
                console.log('pagamento criado');

                var memcachedClient = app.servicos.memcachedClient();
                memcachedClient.set('pagamento-'+pagamento.id, pagamento, 60000, function(erro){
                    console.log('nova chave adicionada ao cache: pagamento- ' + pagamento.id);
                })

                if(pagamento.forma_de_pagamento == 'cartao'){
                    var cartao = req.body["cartao"];
                    console.log(cartao);

                    var clienteCartoes = new app.servicos.clienteCartoes();

                    clienteCartoes.autoriza(cartao, function(exc, request, response, retorno){
                        if(exc) {
                            console.log(exc);
                            res.status(400).send(exc);
                            return;
                        }
                        console.log(retorno);

                        res.location('/pagamentos/pagamento/'+pagamento.id);

                        var response = {
                            dados_do_pagamento: pagamento,
                            cartao: retorno,
                            links: [
                                {
                                    href: "http://localhost:3000/pagamentos/pagamento/"+pagamento.id,
                                    rel: "confirmar",
                                    method: "PUT"
                                },
                                {
                                    href: "http://localhost:3000/pagamentos/pagamento/"+pagamento.id,
                                    rel: "cancelar",
                                    method: "DELETE"
                                }
                            ]
                        }

                        res.status(201).json(response);
                        return;
                    });

                } else {
                    res.location('/pagamentos/pagamento/'+pagamento.id);

                    var response = {
                        dados_do_pagamento: pagamento,
                        links: [
                            {
                                href: "http://localhost:3000/pagamentos/pagamento/"+pagamento.id,
                                rel: "confirmar",
                                method: "PUT"
                            },
                            {
                                href: "http://localhost:3000/pagamentos/pagamento/"+pagamento.id,
                                rel: "cancelar",
                                method: "DELETE"
                            }
                        ]
                    }


                    res.status(201).json(response);
                }


                
            }
        })


        //res.send(pagamento);

    });

}