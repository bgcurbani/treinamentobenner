﻿using System;
using Alura.LeilaoOnline.Core;

namespace Alura.LeilaoOnline.ConsoleApp
{
    class Program
    {
        static void Main()
        {
            LeilaoComVariosLances();
            LeilaoComUmLance();

        }

        private static void LeilaoComUmLance()
        {
            //Cenario
            var leilao = new Leilao("Van Gogh", new MaiorValor());
            var fulano = new Interessada("Fulano", leilao);
            var maria = new Interessada("Fulano", leilao);

            leilao.RecebeLance(fulano, 800);

            //Ato
            leilao.TerminaPregao();

            var valorEsperado = 800;
            var valorObtido = leilao.Ganhador.Valor;
            Verifica(valorEsperado, valorObtido);

        }

        private static void Verifica(double valorEsperado, double valorObtido)
        {
            var cor = Console.ForegroundColor;
            //Assert
            if (valorEsperado == valorObtido)
            {
                Console.ForegroundColor = ConsoleColor.Green;
                Console.WriteLine("Teste OK");
            }
            else
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine($"Teste Falhou! Esperado: {valorEsperado}, obtido: {valorObtido}");
            }

            Console.ForegroundColor = cor;

        }

        private static void LeilaoComVariosLances()
        {
            //Cenario
            var leilao = new Leilao("Van Gogh", new MaiorValor());
            var fulano = new Interessada("Fulano", leilao);
            var maria = new Interessada("Fulano", leilao);

            leilao.RecebeLance(fulano, 800);
            leilao.RecebeLance(maria, 900);
            leilao.RecebeLance(fulano, 1000);
            leilao.RecebeLance(maria, 990);

            //Ato
            leilao.TerminaPregao();


            //Assert
            Console.WriteLine(leilao.Ganhador.Valor);
        }
    }
}
