﻿using Alura.LeilaoOnline.Core;
using Xunit;

namespace Alura.LeilaoOnline.Tests
{
    public class LeilaoTerminaPregao
    {


        [Theory]
        [InlineData(1250, 1200, new double[] { 800, 1150, 1400, 1250 })]
        public void RetornaValorSuperiorMaisProximoDadoLeilaoNessaModalidade(
            double valorEsperado,
            double valorDestino, 
            double[] ofertas)
        {
            IModalidadeAvaliacao modalidade = new OfertaSuperiorMaisProxima(valorDestino);
            var leilao = new Leilao("Van Gogh", modalidade);
            var fulano = new Interessada("Fulano", leilao);
            var maria = new Interessada("Maria", leilao);

            leilao.IniciaPregao();
            for (int i = 0; i < ofertas.Length; i++)
            {

                if ((i % 2) == 0)
                {
                    leilao.RecebeLance(fulano, ofertas[i]);
                }
                else
                {
                    leilao.RecebeLance(maria, ofertas[i]);
                }

            }


            //Act
            leilao.TerminaPregao();


            //Assert
            Assert.Equal(valorEsperado, leilao.Ganhador.Valor);


        }


        [Fact]
        public void LancaInvalidOperationExceptionDadoPregaoNaoIniciado()
        {

            var leilao = new Leilao("Van Gogh", new MaiorValor());


            var excecaoObtida = Assert.Throws<System.InvalidOperationException>(
                () => leilao.TerminaPregao());

            var msgEsperada = "Não é possivel terminar o pregão sem que ele tenha começado. Para isso, utilize o método IniciaPregao().";
            Assert.Equal(msgEsperada, excecaoObtida.Message);
        }

        [Fact]
        public void RetornaZeroDadoLeilaoSemLances()
        {
            var leilao = new Leilao("Van Gogh", new MaiorValor());

            leilao.IniciaPregao();
            leilao.TerminaPregao();


            var valorEsperado = 0;
            var valorObtido = leilao.Ganhador.Valor;

            Assert.Equal(valorEsperado, valorObtido);
        }

        
                     
        [Theory]
        [InlineData(1200, new double[] { 800, 900, 1000, 1200})]
        [InlineData(1000, new double[] { 800, 900, 1000, 990 })]
        [InlineData(800, new double[] { 800 })]
        public void RetornaMaiorValorDadoLeilaoComPeloMenosUmLance(double valorEsperado, double[] ofertas)
        {
            //Cenario
            var modalidade = new MaiorValor();
            var leilao = new Leilao("Van Gogh", modalidade);
            var fulano = new Interessada("Fulano", leilao);
            var maria = new Interessada("Fulano", leilao);

            leilao.IniciaPregao();
            for (int i = 0; i < ofertas.Length; i++)
            {

                if ((i % 2) == 0)
                {
                    leilao.RecebeLance(fulano, ofertas[i]);
                }
                else
                {
                    leilao.RecebeLance(maria, ofertas[i]);
                }

            }

            //Ato
            leilao.TerminaPregao();
            var valorObtido = leilao.Ganhador.Valor;

            //Assert
            Assert.Equal(valorEsperado, valorObtido);
        }

        


    }
}
