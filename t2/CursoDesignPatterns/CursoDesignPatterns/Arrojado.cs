﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns
{
    public class Arrojado : Investimento
    {
        public double Investir()
        {
            int chance = new Random().Next(101);
            if (chance <= 20) return 0.05;
            if (chance <= 50) return 0.03;
            return 0.006;
        }
    }
}
