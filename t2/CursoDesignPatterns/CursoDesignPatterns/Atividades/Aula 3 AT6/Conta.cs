﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns.Atividades.Aula_3_AT6
{
    public class Conta
    {
        public string Titular { get; set; }
        public string Numero { get; set; }
        public string Agencia { get; set; }
        public double Saldo { get; set; }


    }
}
