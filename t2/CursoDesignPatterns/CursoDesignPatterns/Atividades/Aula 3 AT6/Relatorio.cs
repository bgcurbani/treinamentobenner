﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns.Atividades.Aula_3_AT6
{
    public abstract class Relatorio
    {
        
        public abstract void Cabecalho();
        public abstract void Corpo(IList<Conta> contas);
        public abstract void Rodape();

        public void Imprime(IList<Conta> contas)
        {
            Cabecalho();
            Corpo(contas);
            Rodape();
        }
        
    }
}
