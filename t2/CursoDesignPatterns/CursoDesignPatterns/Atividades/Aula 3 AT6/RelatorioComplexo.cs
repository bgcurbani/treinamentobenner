﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns.Atividades.Aula_3_AT6
{
    public class RelatorioComplexo : Relatorio
    {

        public override void Cabecalho()
        {
            Console.WriteLine("Banco 001");
            Console.WriteLine("Endereço: Rua alguma coisa");
            Console.WriteLine("Tel: (47)30003030");
        }

        public override void Corpo(IList<Conta> contas)
        {
            foreach (Conta conta in contas)
            {
                Console.WriteLine(conta.Titular + ", " + conta.Agencia + ", " + conta.Numero + ", " + conta.Saldo.ToString());
            }
        }

        public override void Rodape()
        {
            Console.WriteLine("Email: banco001@banco001.com.br");
            Console.WriteLine(DateTime.Now);
        }
    }


}
