﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns.Atividades.Aula_3_AT6
{
    public class RelatorioSimples : Relatorio
    {
        public override void Cabecalho()
        {
            Console.WriteLine("Banco 001");
        }

        public override void Corpo(IList<Conta> contas)
        {
            foreach (Conta conta in contas)
            {
                Console.WriteLine(conta.Titular + " - " + conta.Saldo);
            }
        }

        public override void Rodape()
        {
            Console.WriteLine("Tel: (47)30003030");
        }
    }
}
