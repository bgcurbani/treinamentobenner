﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns.Atividades.Aula_4_AT___Decorator
{
    public class Conta
    {
        public string Titular { get; set; }
        public string Numero { get; set; }
        public string Agencia { get; set; }
        public double Saldo { get; set; }
        public DateTime DataAbertura { get; set; }

        public Conta(string titular, string numero, string agencia, double saldo, DateTime dataAbertura)
        {
            Titular = titular;
            Numero = numero;
            Agencia = agencia;
            Saldo = saldo;
            DataAbertura = dataAbertura;
        }
    }
}
