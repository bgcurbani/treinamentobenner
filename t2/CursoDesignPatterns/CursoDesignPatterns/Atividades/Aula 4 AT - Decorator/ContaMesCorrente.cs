﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns.Atividades.Aula_4_AT___Decorator
{
    public class ContaMesCorrente : Filtro
    {
        public ContaMesCorrente() { }

        public ContaMesCorrente(Filtro outroFiltro) : base(outroFiltro) { }

        public override IList<Conta> Filtra(IList<Conta> contas)
        {
            List<Conta> c = new List<Conta>();

            foreach (Conta conta in contas)
            {
                if(conta.DataAbertura.Month == DateTime.Now.Month && conta.DataAbertura.Year == DateTime.Now.Year)
                {
                    c.Add(conta);
                }
            }


            return c.Concat(FiltroExtra(contas)).ToList();
        }
    }
}
