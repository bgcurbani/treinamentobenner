﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns.Atividades.Aula_4_AT___Decorator
{
    public abstract class Filtro
    {

        public Filtro outroFiltro { get; set; }
        public Filtro(Filtro outroFiltro) { this.outroFiltro = outroFiltro; }
        public Filtro() { outroFiltro = null; }

        public abstract IList<Conta> Filtra(IList<Conta> contas);

        protected IList<Conta> FiltroExtra(IList<Conta> contas)
        {
            if(outroFiltro == null) { return new List<Conta>(); }
            return outroFiltro.Filtra(contas);
        }


    }
}
