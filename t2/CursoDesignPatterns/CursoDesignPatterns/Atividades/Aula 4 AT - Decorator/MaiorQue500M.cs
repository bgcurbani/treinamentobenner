﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns.Atividades.Aula_4_AT___Decorator
{
    public class MaiorQue500M : Filtro
    {
        public MaiorQue500M() : base() { }

        public MaiorQue500M(Filtro outroFiltro) : base(outroFiltro) { }

        public override IList<Conta> Filtra(IList<Conta> contas)
        {
            List<Conta> c = new List<Conta>();

            foreach (Conta conta in contas)
            {
                if(conta.Saldo > 500000)
                {
                    c.Add(conta);
                }
            }


            return c.Concat(FiltroExtra(contas)).ToList();
        }
    }
}
