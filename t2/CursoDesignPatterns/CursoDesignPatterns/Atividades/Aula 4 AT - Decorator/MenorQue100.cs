﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns.Atividades.Aula_4_AT___Decorator
{
    public class MenorQue100 : Filtro
    {
        public MenorQue100(Filtro outroFiltro) : base(outroFiltro) { }
        public MenorQue100() : base() { }

        public override IList<Conta> Filtra(IList<Conta> contas)
        {
            List<Conta> c = new List<Conta>();

            foreach (Conta conta in contas)
            {
                if(conta.Saldo < 100)
                {
                    c.Add(conta);
                }
            }

            
            return c.Concat(FiltroExtra(contas)).ToList();
        }
    }
}
