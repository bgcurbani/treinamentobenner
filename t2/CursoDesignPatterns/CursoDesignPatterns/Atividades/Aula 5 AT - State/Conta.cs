﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns.Atividades.Aula_5_AT___State
{
    public class Conta
    {
        public string Titular { get; set; }
        public string Numero { get; set; }
        public string Agencia { get; set; }
        public double Saldo { get; set; }
        public EstadoConta EstadoConta { get; set; }

        public Conta(string titular, double saldo)
        {
            Titular = titular;
            Saldo = saldo;
            if (Saldo > 0)
            {
                EstadoConta = new Positivo();
            }
            else EstadoConta = new Negativo();
        }



        public void Sacar(double valor)
        {
            EstadoConta.Saque(this, valor);
        }

        public void Depositar(double valor)
        {
            EstadoConta.Deposito(this, valor);
        }

    }

}


