﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns.Atividades.Aula_5_AT___State
{
    public interface EstadoConta
    {

        void Saque(Conta conta, double valor);
        void Deposito(Conta conta, double valor);

    }
}
