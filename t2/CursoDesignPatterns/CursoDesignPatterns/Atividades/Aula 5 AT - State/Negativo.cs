﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns.Atividades.Aula_5_AT___State
{
    public class Negativo : EstadoConta
    {
        public void Deposito(Conta conta, double valor)
        {
            conta.Saldo += valor * 0.95;
            if (conta.Saldo < 0)
            {
                conta.EstadoConta = new Negativo();
            }
            else conta.EstadoConta = new Positivo();
        }

        public void Saque(Conta conta, double valor)
        {
            throw new Exception("Não é possivel realizar saques de uma conta com saldo negativo");
        }
    }
}
