﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns.Atividades.Aula_5_AT___State
{
    public class Positivo : EstadoConta
    {
        public void Deposito(Conta conta, double valor)
        {
            conta.Saldo += valor * 0.98;
            if (conta.Saldo < 0)
            {
                conta.EstadoConta = new Negativo();
            }
            else conta.EstadoConta = new Positivo();
        }

        public void Saque(Conta conta, double valor)
        {
            conta.Saldo -= valor;
            if (conta.Saldo < 0)
            {
                conta.EstadoConta = new Negativo();
            }
            else conta.EstadoConta = new Positivo();
        }
    }
}
