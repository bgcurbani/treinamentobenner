﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns
{
    public class Conservador : Investimento
    {
        public double Investir()
        {
            return 0.08;
        }
    }
}
