﻿namespace CursoDesignPatterns
{
    public interface Investimento
    {

        double Investir();

    }
}