﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns
{
    public class Moderado : Investimento
    {
        public double Investir()
        {
            bool escolhido = new Random().Next(101) > 50;

            if (escolhido)
            {
                return 0.025;
            }
            else
            {
                return 0.07;
            }
        }
    }
}
