﻿using CursoDesignPatterns.Atividades.Aula_4_AT___Decorator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns
{
    class Program
    {
        static void Main(string[] args)
        {
            NotaFiscalBuilder criador = new NotaFiscalBuilder();
            criador
                .ParaEmpresa("Alura")
                .ComCnpj("12.345.678/0001-12")
                .ComItem(new ItemDaNota("Item 1", 100.0))
                .ComItem(new ItemDaNota("Item 2", 200.0))
                .ComObservacoes("Alguma observação");


            criador.AdicionaAcao(new EnviadorDeEmail());
            criador.AdicionaAcao(new NotaFiscalDAO());
            criador.AdicionaAcao(new EnviadorDeSms());
            criador.AdicionaAcao(new Multiplicador(2));

            NotaFiscal nf = criador.Constroi();

            Console.WriteLine(nf.ValorBruto);
            Console.WriteLine(nf.Impostos);


            Console.ReadKey();

        }

        private static void ExemploState()
        {
            Orcamento reforma = new Orcamento(500);

            reforma.AplicaDescontoExtra();
            Console.WriteLine(reforma.Valor);


            reforma.Aprova();
            reforma.AplicaDescontoExtra();
            Console.WriteLine(reforma.Valor);
            reforma.AplicaDescontoExtra();
            Console.WriteLine(reforma.Valor);

            reforma.Finaliza();
        }

        private static void AL4AT6()
        {
            List<Conta> contas = new List<Conta>();
            contas.Add(new Conta("Bruno50", "123", "321", 50.0, DateTime.Now));
            contas.Add(new Conta("Bruno100", "123", "321", 100.0, DateTime.Now));
            contas.Add(new Conta("Bruno500M", "123", "321", 510000.0, DateTime.Now));

            Filtro f1 = new MenorQue100(new MaiorQue500M());
            IList<Conta> contasFiltradas = f1.Filtra(contas);

            foreach (Conta conta in contasFiltradas)
            {
                Console.WriteLine(conta.Titular);
            }
        }

        private static void ExemploDecorator()
        {
            Imposto iss = new ISS(new ImpostoMuitoAlto());

            Orcamento orcamento = new Orcamento(500);

            double valor = iss.Calcula(orcamento);

            Console.WriteLine(valor);
        }

        private static void ImpostoIMP()
        {
            Orcamento orc = new Orcamento(600);
            orc.AdicionaItem(new Item("Caneta", 200));
            orc.AdicionaItem(new Item("Grafite", 200));
            orc.AdicionaItem(new Item("Borracha", 100));
            orc.AdicionaItem(new Item("Lapis", 100));


            Imposto imp = new IKCV();

            double imposto = imp.Calcula(orc);

            Console.WriteLine(imposto);
        }

        private static void CalculadorDesconto()
        {
            CalculadorDeDescontos calculador = new CalculadorDeDescontos();

            Orcamento orcamento = new Orcamento(500);
            orcamento.AdicionaItem(new Item("Caneta", 250));
            orcamento.AdicionaItem(new Item("Lapis", 250));
            orcamento.AdicionaItem(new Item("a", 250));
            orcamento.AdicionaItem(new Item("b", 250));
            orcamento.AdicionaItem(new Item("c", 250));


            double desconto = calculador.Calcula(orcamento);

            Console.WriteLine(desconto);
        }
    }
}
