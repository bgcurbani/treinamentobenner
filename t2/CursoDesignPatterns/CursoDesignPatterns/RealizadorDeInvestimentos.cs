﻿using CursoDesignPatterns.Atividades.Aula_3_AT6;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CursoDesignPatterns
{
    public class RealizadorDeInvestimentos
    {

        public void Investe(Investimento tipo, Conta conta)
        {
            conta.Saldo *= tipo.Investir();
        }

    }
}
