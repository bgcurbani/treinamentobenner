﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DesignPatterns2.Cap5;

namespace DesignPatterns2.Cap4
{
    public class RaizQuadrada : IExpressao
    {

        private IExpressao numero;

        public RaizQuadrada(IExpressao numero)
        {
            this.numero = numero;
        }

        public void Aceita(IVisitor impressora)
        {
            throw new NotImplementedException();
        }

        public int Avalia()
        {
            double valor = numero.Avalia();
            return (int) Math.Sqrt(valor);
        }
    }
}
