﻿using DesignPatterns2.Cap4;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns2.Cap5
{
    public interface IVisitor
    {

        void ImprimeSoma(Soma soma);

        void ImprimeSubtracao(Subtracao sub);

        void ImprimeNumero(Numero numero);

        void ImprimeMultiplicacao(Multiplicacao mult);

        void ImprimeDivisao(Divisao divisao);

    }
}
