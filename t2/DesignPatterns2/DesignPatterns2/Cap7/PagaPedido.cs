﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DesignPatterns2.Cap7
{
    public class PagaPedido : IComando
    {
        private Pedido pedido;

        public void Executa()
        {
            Console.WriteLine("Pagando pedido do cliente {0}", pedido.Cliente);
            pedido.Paga();
        }

        public PagaPedido(Pedido pedido)
        {
            this.pedido = pedido;
        }
    }
}
