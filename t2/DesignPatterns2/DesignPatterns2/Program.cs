﻿using DesignPatterns2.Cap1;
using DesignPatterns2.Cap2;
using DesignPatterns2.Cap3;
using DesignPatterns2.Cap4;
using DesignPatterns2.Cap5;
using DesignPatterns2.Cap6;
using DesignPatterns2.Cap7;
using DesignPatterns2.Cap8;
using DesignPatterns2.Cap9;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace DesignPatterns2
{
    public class Program
    {
        static void Main(string[] args)
        {

            String cpf = "1234";

            EmpresaFacade facade = new EmpresaFacadeSingleton().Instancia;
            Cliente cliente = facade.BuscaCliente(cpf);
            var fatura = facade.CriaFatura(cliente, 5000);
            facade.GeraCobranca(tipo.Boleto, fatura);



        }

        private static void Adapter()
        {
            //Padrão parecido com strategy e command, porém com finalidade diferente
            Cliente cliente = new Cliente();
            cliente.Nome = "Bruno";
            cliente.Endereco = "Rua Alguma";
            cliente.DataNascimento = DateTime.Now;

            String xml = new GeradorDeXml().GeraXml(cliente);

            Console.WriteLine(xml);
        }

        private static void Command()
        {

            //Guarda uma fila de comandos para ser executando no futuro
            FilaDeTrabalho fila = new FilaDeTrabalho();
            Pedido pedido1 = new Pedido("Bruno", 430.0);
            Pedido pedido2 = new Pedido("André", 450.0);

            fila.Adiciona(new PagaPedido(pedido1));
            fila.Adiciona(new PagaPedido(pedido2));

            fila.Adiciona(new FinalizaPedido(pedido1));

            fila.Processa();
        }

        private static void Bridge()
        {
            IMensagem mensagem = new MensagemCliente("Victor");
            IEnviador enviador = new EnviaPorEmail();
            mensagem.Enviador = enviador;
            mensagem.Envia();
        }

        private static void Visitor()
        {

            //(1 + 10) + (20 - 10)
            IExpressao esquerda = new Soma(new Numero(1), new Numero(10));
            IExpressao direita = new Subtracao(new Numero(20), new Numero(10));
            IExpressao soma = new Soma(esquerda, direita);

            Console.WriteLine(soma.Avalia());

            ImpressoraVisitor impressora = new ImpressoraVisitor();
            soma.Aceita(impressora);
        }

        private static void Cap3()
        {
            Historico historico = new Historico();
            Contrato c = new Contrato(DateTime.Now, "Bruno", TipoContrato.Novo);
            historico.Adiciona(c.salvaEstado());


            c.Avanca();
            historico.Adiciona(c.salvaEstado());
            Console.WriteLine(c.Tipo);


            c.Avanca();
            historico.Adiciona(c.salvaEstado());
            Console.WriteLine(c.Tipo);

            Console.WriteLine(historico.Pega(0).Contrato.Tipo);
        }

        private static void Cap2()
        {
            NotasMusicais notas = new NotasMusicais();

            IList<INota> musica = new List<INota>()
            {
                notas.Pega("do"), notas.Pega("re"), notas.Pega("mi"), notas.Pega("fa"), notas.Pega("fa"), notas.Pega("fa")
            };


            Piano piano = new Piano();
            piano.Toca(musica);
        }
    }
}
