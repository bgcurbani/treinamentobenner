﻿namespace Solid.Cap1
{
    public interface IRegraDeCalculo
    {
        double Calcula(Funcionario funcionario);

    }
}