﻿using System;

namespace Solid.Cap1
{
    public class QuinzeOuVinteCincoPorcento : IRegraDeCalculo
    {
        public double Calcula(Funcionario funcionario)
        {
            if(funcionario.SalarioBase > 2000.0)
            {
                return funcionario.SalarioBase * 0.65;
            }
            else
            {
                return funcionario.SalarioBase * 0.85;
            }
        }
    }
}