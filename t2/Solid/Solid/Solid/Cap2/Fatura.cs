﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid.Cap2
{
    public class Fatura
    {
        private int valor;
        private string cliente;

        public Fatura(int valor, string cliente)
        {
            this.valor = valor;
            this.cliente = cliente;
        }

        public double ValorMensal { get; set; }
    }
}
