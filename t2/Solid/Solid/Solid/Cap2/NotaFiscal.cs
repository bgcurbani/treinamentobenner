﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid.Cap2
{
    public class NotaFiscal
    {
        public double ValorBruto { get; set; }
        public double Impostos { get; set; }
        public double ValorLiquido
        {
            get
            {
                return this.ValorBruto - this.Impostos;
            }
        }

        public NotaFiscal(double valor, double impostos)
        {
            this.ValorBruto = valor;
            this.Impostos = impostos;
        }

        
    }
}
