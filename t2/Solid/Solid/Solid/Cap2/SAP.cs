﻿using System;

namespace Solid.Cap2
{
    internal class SAP : IAcaoAposGerarNota
    {
        public void Executa(NotaFiscal nf)
        {
            Console.WriteLine("Envia para SAP");
        }
    }
}