﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid.Cap3
{
    public class CalculadoraDePrecos
    {
        private ITabelaDePreco Tabela { get; set; }
        private IServicoDeEntrega Entrega { get; set; }

        public CalculadoraDePrecos(ITabelaDePreco tabela, IServicoDeEntrega entrega)
        {
            this.Tabela = tabela;
            this.Entrega = entrega;
        }


        public double Calcula(Compra produto)
        {
            TabelaDePrecoPadrao tabela = new TabelaDePrecoPadrao();
            Frete correios = new Frete();

            double desconto = Tabela.DescontoPara(produto.Valor);
            double frete = Entrega.Para(produto.Cidade);

            return produto.Valor * (1 - desconto) + frete;
        }

    }
}
