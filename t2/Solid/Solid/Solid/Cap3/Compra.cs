﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid.Cap3
{
    public class Compra
    {

        public double Valor { get; private set; }
        public string Cidade { get; private set; }

        public Compra(int valor, string cidade)
        {
            this.Valor = valor;
            this.Cidade = cidade;
        }

        
    }
}
