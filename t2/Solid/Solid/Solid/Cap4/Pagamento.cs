﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid.Cap4
{
    public class Pagamento
    {
        
        public double Valor { get; private set; }
        public MeioDePagamento Tipo { get; set; }

        public Pagamento(double valor, MeioDePagamento tipo)
        {
            this.Valor = valor;
            this.Tipo = tipo;
        }

    }
}
