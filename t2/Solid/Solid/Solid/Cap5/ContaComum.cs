﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid.Cap5
{
    public class ContaComum
    {
        private ManipuladorDeSaldo manipulador;
        public double Saldo { get => manipulador.Saldo; }


        public ContaComum(int valor)
        {
            this.manipulador = new ManipuladorDeSaldo();
            this.manipulador.Deposita(valor);
        }


        public virtual void Deposita(double valor)
        {
            manipulador.Deposita(valor);
        }


        public void Saca(double valor)
        {
            manipulador.Saca(valor);
        }

        public void SomaInvestimento()
        {
            this.manipulador.SomaInvestimento(1.1);
        }


    }
}
