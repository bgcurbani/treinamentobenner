﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Solid.Cap5
{
    public class ContaEstudante : ContaComum
    {

        private ManipuladorDeSaldo manipulador { get; set; }
        public int Milhas { get; private set; }

        public ContaEstudante(int valor) : base(valor)
        { 
            this.manipulador = new ManipuladorDeSaldo();
            this.manipulador.Deposita(valor);
        }

        public override void Deposita(double valor)
        {
            manipulador.Deposita(valor);
            this.Milhas += (int)valor;
        }

    }
}
