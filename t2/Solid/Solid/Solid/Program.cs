﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Solid.Cap1;
using Solid.Cap2;
using Solid.Cap3;
using Solid.Cap5;

namespace Solid
{
    class Program
    {
        static void Main(string[] args)
        {
            IList<ContaComum> contas = ContasDoBanco();
            foreach (ContaComum conta in contas)
            {
                conta.SomaInvestimento();
                Console.WriteLine("Novo saldo: " + conta.Saldo);
            }

        }

        private static IList<ContaComum> ContasDoBanco()
        {
            List<ContaComum> contas = new List<ContaComum>();
            contas.Add(new ContaComum(100));
            contas.Add(new ContaComum(150));
            contas.Add(new ContaEstudante(100));

            return contas;
        }

        private static void Cap3()
        {
            Compra compra = new Compra(500, "sao paulo");
            CalculadoraDePrecos calc = new CalculadoraDePrecos(new TabelaDePrecoPadrao(), new Transportadora());

            double resultado = calc.Calcula(compra);
            Console.WriteLine("O preço de compra e : " + resultado);
            Console.ReadKey();
        }

        private static void Cap2()
        {
            IList<IAcaoAposGerarNota> acoes = new List<IAcaoAposGerarNota>();
            acoes.Add(new EnviadorDeEmail());
            acoes.Add(new NotaFiscalDao());
            acoes.Add(new SAP());
            GeradorDeNotaFiscal gnf = new GeradorDeNotaFiscal(acoes);
            Fatura fatura = new Fatura(200, "Algum nome");
        }

        private static void Cap1()
        {
            CalculadoraDeSalario cs = new CalculadoraDeSalario();
            Funcionario funcionario = new Funcionario(new Desenvolvedor(new DezOuVintePorcento()), 2000);

            double resultado;

            resultado = cs.Calcula(funcionario);
            Console.WriteLine(resultado);
        }
    }
}
