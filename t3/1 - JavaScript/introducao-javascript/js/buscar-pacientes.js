var botaoAdicionar = document.querySelector("#buscar-pacientes");

botaoAdicionar.addEventListener("click", function () {
    //http://api-pacientes.herokuapp.com/pacientes

    var xhr = new XMLHttpRequest();

    xhr.open("GET", "http://api-pacientes.herokuapp.com/pacientes");

    xhr.addEventListener("load", function () {
        document.querySelector("#erro-ajax").classList.remove("invisivel");
        if (xhr.status == 200) {
            document.querySelector("#erro-ajax").classList.add("invisivel");
            var pacientes = JSON.parse(xhr.responseText);
            pacientes.forEach(paciente => {
                adicionaPacienteNaTabela(paciente);
            });
        } else {
            console.log(xhr.status + " " + xhr.statusText);
            console.log(xhr.responseText);
            document.querySelector("#erro-ajax").classList.remove("invisivel");
        }

    });

    xhr.send();


});