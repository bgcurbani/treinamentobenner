atualizaPacientes();

var titulo = document.querySelector(".titulo");
titulo.textContent = "Aparecida Nutricionista";
titulo.addEventListener("click", function () {
    alert("Olá, eu fui clicado!");
});





function atualizaPacientes() {
    var pacientes = document.querySelectorAll(".paciente");
    //var paciente = document.querySelector("#primeiro-paciente");

    pacientes.forEach(paciente => {
        calculaImcPaciente(paciente);
    });
}

function calculaImc(peso, altura){
    var imc = 0;
    imc = peso / (altura * altura);
    return imc.toFixed(2);

}


function calculaImcPaciente(paciente) {
    var tdPeso = paciente.querySelector(".info-peso");
    var tdAltura = paciente.querySelector(".info-altura");
    var tdImc = paciente.querySelector(".info-imc");

    var peso = tdPeso.textContent;
    var altura = tdAltura.textContent;

    var pesoEhValido = validaPeso(peso);
    var alturaEhValido = validaAltura(altura);

    if (!pesoEhValido) {
        console.log("Peso inválido");
        tdImc.textContent = "Peso inválido";
        paciente.classList.add("paciente-invalido");
    }

    if (!alturaEhValido) {
        console.log("Altura inválida!");
        tdImc.textContent = "Altura inválido";
        paciente.classList.add("paciente-invalido");
    }

    if (pesoEhValido && alturaEhValido) {
        var imc = calculaImc(peso, altura);
        tdImc.textContent = imc;
    }

}

function validaPeso(peso){
    if(peso >= 0 && peso < 1000){
        return true;
    } else {
        return false;
    }
}
function validaAltura(altura) {
    if(altura >= 0 && altura <= 3.0){
        return true;
    } else {
        return false;
    }
}
