$("#botaoFrase").click(fraseAleatoria);
$("#botaoFraseId").click(buscaFrase);

function fraseAleatoria() {
    $("#spinner").show();
    $.get("http://localhost:3000/frases", trocaFraseAleatoria)
        .fail(function () {
            $("#erro").show();
            setTimeout(function () {
                $("#erro").toggle();
            }, 2000);
        })
        .always(function () {
            $("#spinner").toggle();
        });
}


function trocaFraseAleatoria(data) {
    //$("#erro").hide();
    var numeroAleatorio = Math.floor(Math.random() * data.length);

    $(".frase").text(data[numeroAleatorio].texto);
    //$("#tamanhoFrase").text(data[numeroAleatorio].texto.length);
    atualizaTamanhoFrase();
    atualizaTempoInicial(data[numeroAleatorio].tempo);
}

function buscaFrase() {
    $("#spinner").show();
    var fraseId = $("#fraseId").val();
    var dados = { id: fraseId };

    $.get("http://localhost:3000/frases", dados, trocaFrase)
        .fail(function () {
            $("#erro").toggle();
            setTimeout(function(){
                $("#erro").toggle();
            }, 2000);
        })
        .always(function(){
            $("#spinner").toggle();
        });
}

function trocaFrase(data) {
    $(".frase").text(data.texto);
    atualizaTamanhoFrase();
    atualizaTempoInicial(data.tempo);
}