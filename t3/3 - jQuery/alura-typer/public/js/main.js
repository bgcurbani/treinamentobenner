var tempoInicial = $("#tempoDigitacao").text();
var campo = $(".campoDigitacao");

$(function () {
    atualizaTamanhoFrase();
    inicializaContadores();
    inicializaCronometro();
    inicializaMarcadores();
    atualizaPlacar();
    $('#usuarios').selectize({
        create: true,
        sortField: 'text'
    });
    $('.tooltip').tooltipster({
        trigger: "custom"
    });
    $("#botaoReiniciar").click(reiniciaJogo);
});

function atualizaTamanhoFrase() {
    var frase = $(".frase").text();
    var qtdePalavras = frase.split(" ").length;
    var tamanhoFrase = $("#tamanhoFrase");
    tamanhoFrase.text(qtdePalavras);
}

function atualizaTempoInicial(tempo) {
    tempoInicial = tempo;
    $("#tempoDigitacao").text(tempo);
}

function inicializaContadores() {
    campo.on("input", function () {
        var qtdePalavrasCampo = campo.val().split(/\S+/).length - 1;
        $("#contadorPalavras").text(qtdePalavrasCampo);
        $("#contadorCaracteres").text(campo.val().length);
    });
}

function inicializaCronometro() {
    campo.one("focus", function () {
        var tempoRestante = $("#tempoDigitacao").text();
        $("#botaoReiniciar").attr("disabled", true);
        var cronometroID = setInterval(function () {
            tempoRestante--;
            if (tempoRestante < 1) {
                clearInterval(cronometroID);
                finalizaJogo();
            }
            $("#tempoDigitacao").text(tempoRestante);
            console.log(tempoRestante);
        }, 1000)
    });
}

function finalizaJogo() {
    campo.attr("disabled", true);
    $("#botaoReiniciar").attr("disabled", false);
    campo.toggleClass("campoDesativado");
    inserePlacar();
}

function inicializaMarcadores() {
    campo.on("input", function () {
        var frase = $(".frase").text();
        var digitado = campo.val();
        var comparavel = frase.substr(0, digitado.length);

        if (digitado == comparavel) {
            campo.removeClass("bordaInvalida");
            campo.addClass("bordaValida");
        } else {
            campo.addClass("bordaInvalida");
            campo.removeClass("bordaValida");
        }

    });
}

function reiniciaJogo() {
    campo.attr("disabled", false);
    campo.val("");
    $("#contadorCaracteres").text("0");
    $("#contadorPalavras").text("0");
    $("#tempoDigitacao").text(tempoInicial);
    campo.toggleClass("campoDesativado");
    campo.removeClass("bordaInvalida");
    campo.removeClass("bordaValida");
    inicializaCronometro();
}