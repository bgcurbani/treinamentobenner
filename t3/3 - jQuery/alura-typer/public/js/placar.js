$("#botaoPlacar").click(mostraPlacar);
$("#botaoSync").click(sincronizaPlacar);

function inserePlacar() {
    var tabela = $(".placar").find("tbody");
    var usuario = $("#usuarios").val();
    var palavras = $("#contadorPalavras").text();    

    var linha = novaLinha(usuario, palavras);
    linha.find(".botaoRemover").click(removeLinha);

    tabela.prepend(linha);
    $(".placar").slideDown(600);
    scrollPlacar();
}

function scrollPlacar() {
    var posicaoPlacar = $(".placar").offset().top;
    $("body").animate({
        scrollTop: posicaoPlacar+"px"
    }, 1000);
}

function novaLinha(usuario, palavras) {
    var linha = $("<tr>");
    var tdUsuario = $("<td>").text(usuario);
    var tdPalavras = $("<td>").text(palavras);
    var tdRemover = $("<td>");
    var link = $("<a>").addClass("botaoRemover").attr("href","#");
    var icone = $("<i>").addClass("small").addClass("material-icons").text("delete");
    link.append(icone);
    tdRemover.append(link);

    linha.append(tdUsuario);
    linha.append(tdPalavras);
    linha.append(tdRemover);

    return linha;

}
function removeLinha() {
    event.preventDefault();
    var linha = $(this).parent().parent();
    //linha.fadeIn();
    //linha.fadeToggle();
    linha.fadeOut();
    setTimeout(function() {
        linha.remove();
    }, 1000);
}

function mostraPlacar() {
    //$(".placar").toggle();
    //$(".placar").slideDown();
    $(".placar").stop().slideToggle(600);    
}

function sincronizaPlacar() {
    var placar = [];
    var linhas = $("tbody>tr");

    linhas.each(function(){
        var usuario = $(this).find("td:nth-child(1)").text();
        var pontuacao = $(this).find("td:nth-child(2)").text();

        var score = {usuario: usuario, pontos:pontuacao};

        placar.push(score);
    });

    var dados = {placar: placar}

    $.post("http://localhost:3000/placar", dados, function(){
        console.log("Salvou no servidor");
        $(".tooltip").tooltipster("open").tooltipster("content", "Sucesso ao sincronizar");
    })
    .fail(()=>{
        $(".tooltip").tooltipster("open").tooltipster("content", "Falha ao sincronizar");
    })
    .always(function(){
        setTimeout(function() {
            $(".tooltip").tooltipster("close");
        }, 1200);
    });

}

function atualizaPlacar() {
    $.get("http://localhost:3000/placar", function(data){
        $(data).each(function(){
            var linha = novaLinha(this.usuario, this.pontos);
            linha.find(".botaoRemover").click(removeLinha);
            $("tbody").append(linha);
        });
    });
}