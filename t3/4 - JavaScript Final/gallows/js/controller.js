const criaController = jogo => {

    const entrada = $("#entrada");
    const lacuna = $(".lacunas");

    const exibeLacunas = () => {

        lacuna.empty();
        var lacunas = jogo.getLacunas();
        lacunas.forEach(lac => {
            var li = $("<li>").addClass("lacuna").text(lac);
            lacuna.append(li);
        });
    };


    const mudaPlaceHolder = texto => entrada.val('').attr('placeholder', texto);

    const guardaPalavraSecreta = () => {

        try {
            jogo.setPalavraSecreta(entrada.val());
            entrada.val('');
            exibeLacunas();
            mudaPlaceHolder('chute');
        } catch (err) {
            alert(err.message);
        }


    };

    const reinicia = () => {
        lacuna.empty();
        jogo.reinicia();
        mudaPlaceHolder('insira uma palavra secreta');
    }

    const leChute = () => {

        try {
            jogo.processaChute(entrada.val().trim().substr(0, 1));
            entrada.val('');
            exibeLacunas();


            if (jogo.ganhouOuPerdeu()) {

                setTimeout(() => {
                    if (jogo.ganhou()) {
                        alert("Parabéns você ganhou!");
                    }

                    if (jogo.perdeu()) {
                        alert("Infelizmente você perdeu!");
                    }
                    reinicia();
                }, 100);

            }
        } catch (err) {
            alert(err.message);
        }

    };

    const inicia = () => {

        entrada.keypress(function (event) {
            if (event.which == 13) {
                switch (jogo.getEtapa()) {
                    case 1:
                        guardaPalavraSecreta();
                        break;
                    case 2:
                        leChute();
                        break;
                }
            }

        });
    };

    return { inicia: inicia };
};
