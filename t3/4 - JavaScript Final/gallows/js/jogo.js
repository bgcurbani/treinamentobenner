const criaJogo = sprite => {

    const ganhou = () => {
        var palavraFinal = letrasPalavra.join("");
        if(palavraOriginal == palavraFinal){
            return true;
        } else {
            return false;
        }
    }

    const perdeu = () =>  sprite.isFinished();

    const ganhouOuPerdeu = () =>  ganhou() || perdeu();

    const reinicia = () => {
        etapa = 1;
        palavraSecreta = palavraOriginal;
        criaLacunas();
        sprite.reset();
    }

    const processaChute = chute => {

        if(!chute.trim()) throw Error('Chute invalido');

        if (!existeLacuna(chute)) {
            if (palavraSecreta.includes(chute)) {
                var idx;
                while ((idx = palavraSecreta.indexOf(chute)) >= 0) {
                    palavraSecreta = palavraSecreta.replace(chute, '*');
                    letrasPalavra[idx] = chute;
                }
            } else {
                if (!sprite.isFinished()) {
                    sprite.nextFrame();
                } else {
                    perdeu();
                }
            }
        } else {
            alert("chute já feito");
        }
    };

    const existeLacuna = chute => {
        for (let i = 0; i < letrasPalavra.length; i++) {
            if (chute == letrasPalavra[i]) {
                return true;
            }
        }

        return false;
    }

    const criaLacunas = () => {
        letrasPalavra = [];
        for (let i = 0; i < palavraSecreta.length; i++) {
            letrasPalavra.push('');
        }
    };

    const proximaEtapa = () => {
        etapaAtual = 2;
    }

    const getEtapa = () => {
        return etapaAtual;
    };

    const setPalavraSecreta = palavra => {
        //letrasPalavra = palavra.split('');
        if(!palavra.trim()) throw Error('Palavra secreta invalida');
        palavraSecreta = palavra;
        palavraOriginal = palavra;
        criaLacunas();
        proximaEtapa();
    };

    const getLacunas = () => {
        return letrasPalavra;
    };

    var palavraOriginal = undefined;
    var palavraSecreta = '';
    var letrasPalavra = [];
    var etapaAtual = 1;


    return {
        getEtapa,
        setPalavraSecreta,
        getLacunas,
        processaChute,
        ganhou,
        perdeu,
        ganhouOuPerdeu,
        reinicia
    }

}