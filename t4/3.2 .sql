CREATE DATABASE curso_sql;

use curso_sql;


CREATE TABLE funcionarios (
	id int unsigned not null auto_increment,
    nome VARCHAR(45) not null,
    salario double not null default '0',
    departamento varchar(45) not null ,
    primary key (id)
);

CREATE TABLE veiculos (
	id int unsigned not null auto_increment,
    funcionario_id int unsigned,
    veiculo varchar(45) not null default '',
    placa varchar(10) not null default '',
    primary key(id),
    CONSTRAINT fk_veiculos_funcionarios FOREIGN KEY (funcionario_id) REFERENCES funcionarios(id)
);

CREATE TABLE salarios (
	faixa varchar(45) not null,
    inicio double not null,
    fim double not null,
    PRIMARY KEY(faixa)
);

ALTER TABLE funcionarios CHANGE COLUMN nome nome_func varchar(50) not null;
ALTER TABLE funcionarios CHANGE COLUMN nome_func nome varchar(45) not null;
-- ALTER TABLE veiculos CHANGE COLUMN funcionario_id funcionario_id int unsigned;

DROP TABLE salarios;

CREATE INDEX departamentos ON funcionarios (departamento);
CREATE INDEX nomes on funcionarios (nome(6));


