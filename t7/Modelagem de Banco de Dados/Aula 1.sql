CREATE INDEX indice_por_lancamento on livros(data_de_lancamento);

select *, 
(select count(l2.data_de_lancamento) 
	from livros as l2 
    where l2.data_de_lancamento = l.data_de_lancamento) as anteriores 
from livros as l 
order by l.data_de_lancamento;

show index from livros;

select * from livros where preco > 20;

explain select * from livros where preco > 20;