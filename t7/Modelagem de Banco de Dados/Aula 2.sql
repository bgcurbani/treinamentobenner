create table usuarios (id integer primary key auto_increment not null, nome varchar(50), email_1 varchar(100), email_2 varchar(100), telefone varchar(15));

insert into usuarios (nome, email_1, email_2, telefone)
values ('Lázaro Jr', 'lazaro@alura.com.br', 'lazaro@gmail.com', '41988990099');

insert into usuarios (nome, email_1, email_2, telefone)
values ('Caio Souza', 'caio@alura.com.br', 'caio@gmail.com', '11999880099');

insert into usuarios (nome, email_1, email_2, telefone)
values ('Alex Felipe', 'alex@alura.com.br', 'alex@gmail.com.br', '11988876677');

INSERT INTO usuarios(nome, email_1, email_2, telefone) 
values('Camila Castro', 'camila@alura.com.br', 'camila@gmail.com', '11988009900');

INSERT INTO usuarios(nome, email_1, email_2, telefone) 
values('Alberto Souza', 'alberto@alura.com.br', 'alberto@gmail.com', '11989890909');

-- muitas colunas para guardar valores semelhantes
select * from usuarios;

alter table usuarios add column (email_3 varchar(100));

alter table usuarios add column (email_4 varchar(100));

alter table usuarios add column (email_5 varchar(100));

alter table usuarios add column (email_concatenado varchar(500));

insert into usuarios (nome, email_1, email_2, telefone, email_concatenado)
values ('João da Silva', 'joao@alura.com.br', 'joao@gmail.com.br', '11999004433','joao@yeahoo.com.br/js@alura.com.br/joao@uol.com.br');

create table emails (
	id integer primary key auto_increment not null,
    usuario_id integer not null,
    email varchar(100) not null,
    foreign key (usuario_id) references usuarios(id));


insert into emails(usuario_id, email) 
values (1, 'lazaro@yahoo.com.br');

select * from emails;

insert into emails(usuario_id, email) 
values (1, 'lpj@gmail.com');

select u.nome, e.email from emails as e join usuarios u on u.id = e.usuario_id;

