select * from compras where valor > 200 and valor <= 700;

select * from compras where valor between 200 and 700;

select * from compras where data between '01-jan-2010' and '01-abr-2010';

update compras set valor = 900 where id = 40;

update compras set valor = 100, observacoes = 'uma compra comum' where data not between '01-jan-2010' and '01-abr-2010';

update compras set observacoes = 'datas festivas' where data = '12-out-2010' or data = '25-dez-2010' or data = '12-jun-2010';

update compras set observacoes = 'datas festivas' where data in ('12-out-2010', '25-dez-2010', '12-jun-2010');

delete from compras where id = 64;