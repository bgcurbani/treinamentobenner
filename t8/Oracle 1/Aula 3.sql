describle compras;
desc compras;

insert into compras (id, valor, data, observacoes, recebido) values (id_seq.nextval, 500, '05-ago-2015', null, '1');

select * from compras where observacoes is null;

select * from compras where observacoes is not null;

delete from compras where observacoes is null;

create table pessoas (nome varchar2(100) not null, ...);

alter table compras modify (observacoes varchar2(30) not null);

alter table compras modify (recebido char default '0' check (recebido in (0,1)));

insert into compras (id, valor, data, observacoes) values (id_seq.nextval, 500, '05-ago-2015', 'presente pro filho');

alter table compras add (forma_pagt varchar2(10) check (forma_pagt in ('boleto', 'dinheiro','cartao')));

insert into compras (id, valor, data, observacoes, forma_pagt) values (id_seq.nextval, 500, '05-ago-2015', 'presente pra mãe', 'boleto');

update compras set forma_pagt = 'dinheiro' where forma_pagt is null;

alter table compras rename column forma_pagt to forma_pagto;

