select sum(valor) from compras;

select sum(valor) from compras where data : '01-JAN-2010';

select avg(valor) from compras;

select count(id) from compras where data between '01-jan-2010' and '31-dez-2010';

select sum(valor), avg(valor) from compras;

select sum(valor) as soma, avg(valor) as media from compras;

select sum(valor) from compras where recebido = '1';

select sum(valor) from compras group by recebido;

select recebido, sum(valor) from compras group by recebido;

select extract(year from data), sum(valor) from compras group by extract(year from data);

select extract(year from data) as ano, sum(valor) as soma from compras group by extract(year from data) order by ano;