create table compradores (id number primary key, nome varchar2(30) not null, endereco varchar2(30) not null, telefone varchar2(20) not null);

create sequence id_compradores_seq;

insert into compradores(id, nome, endereco, telefone) values (id_compradores_seq.nextval,'Flávio', 'Rua do Ouvidor, 123', '(21) 1111-1111');

insert into compradores(id, nome, endereco, telefone) values (id_compradores_seq.nextval,'Nico', 'Av. Presidente Vargas, 123', '(21) 2222-2222');

alter table compras add(comprador_id number);

update compras set comprador_id = 1 where id <= 40;

update compras set comprador_id = 2 where id > 40;

select observacoes, valor, nome from compras join compradores on compras.comprador_id = compradores.id;

alter table compras add foreign key (comprador_id) references compradores(id);

insert into compras(id, valor, data, observacoes, forma_pagto, comprador_id) 
values (id_seq.nextval, 500, '10-jan-2010', 'compras de janeiro', 'cartao', 100);

-----------------------------------------------------------------------------------

select compradores.nome, compras.valor from compras
join compradores on compras.comprador_id = compradores.id
where compras.data < '09-jul-2010';

select compras.* from compras
join compradores on compras.comprador_id = compradores.id
where compradores.id = 1;

select compras.* from compras
join compradores on compras.comprador_id = compradores.id
where compradores.nome like 'Flávio%';

select compras.* from compras
join compradores on compras.comprador_id = compradores.id
where compradores.nome like 'FLAVIO%';

select compradores.nome, sum(compras.valor) from compras
join compradores on compras.comprador_id = compradores.id
group by compradores.nome;