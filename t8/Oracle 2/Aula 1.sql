select table_name from user_tables;

desc matricula;

select aluno.nome, curso.nome from aluno
join matricula on matricula.aluno_id = aluno.id
join curso on curso.id = matricula.curso_id;

select a.nome, c.nome from aluno a
join matricula m on m.aluno_id = a.id
join curso c on m.curso_id = c.id;

select a.nome from aluno a where not exists(select m.id from matricula m where m.aluno_id = a.id);

select * from exercicio e where not exists(select r.id from resposta r where r.exercicio_id = e.id);

select c.nome from curso c where not exists(select m.id from matricula m where m.curso_id = c.id);

---------------------------------------------------------------------------------------------------

select a.nome from aluno a where not exists(select m.id from matricula m where (not m.data between '01-jan-2015' and '31-dez-2015') and a.id = m.aluno_id);