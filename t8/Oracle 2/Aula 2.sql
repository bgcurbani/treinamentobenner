select c.nome, avg(n.nota) from nota n
join resposta r on r.id = n.resposta_id
join exercicio e on e.id = r.exercicio_id
join secao s on s.id = e.secao_id
join curso c on c.id = s.curso_id
group by c.nome;


select c.nome, count(e.id) as qtde from exercicio e
join secao s on s.id = e.secao_id
join curso c on c.id = s.curso_id
group by c.nome;

select c.nome, count(a.id) from curso c
join matricula m on m.curso_id = c.id
join aluno a on a.id = m.aluno_id
group by c.nome;



select a.nome, c.nome, avg(n.nota) from nota n
join resposta r on r.id = n.resposta_id
join exercicio e on e.id = r.exercicio_id
join secao s on s.id = e.secao_id
join curso c on c.id = s.curso_id
join matricula m on m.curso_id = c.id
join aluno a on a.id = m.aluno_id
where a.nome like '%Silva' or a.nome like'%Santos'
group by a.nome, c.nome;

select e.pergunta, count(r.id) from exercicio e
join resposta r on e.id = r.exercicio_id
group by e.pergunta
