select distinct tipo from matricula;


select c.nome, m.tipo, count(m.id) as qtde from matricula m
join curso c on c.id = m.curso_id
where m.tipo = 'PAGA_PF' or m.tipo = 'PAGA_PJ'
group by c.nome, m.tipo;


select c.nome, m.tipo, count(m.id) as qtde from matricula m
join curso c on c.id = m.curso_id
where m.tipo in ('PAGA_PF', 'PAGA_PJ', 'PAGA_BOLETO')
group by c.nome, m.tipo;


select a.nome, c.nome from curso c
join matricula m on c.id = m.curso_id
join aluno a on a.id = m.aluno_id
where a.id in (1,3,4)
order by a.nome;


select a.nome, c.nome from curso c
join matricula m on m.curso_id = c.id
join aluno a on a.id = m.aluno_id
where c.id in (1,9);


select e.pergunta, count(r.id) from exercicio e
join resposta r on r.exercicio_id = e.id
join secao s on s.id = e.secao_id
join curso c on c.id = s.curso_id
where c.id in (1,3)
group by e.pergunta;