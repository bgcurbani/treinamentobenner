select a.nome, c.nome, avg(n.nota) from nota n
join resposta r on r.id = n.resposta_id
join exercicio e on e.id = r.exercicio_id
join secao s on s.id = e.secao_id
join curso c on c.id = s.curso_id
join aluno a on a.id = r.aluno_id
group by c.nome, a.nome;


select avg(n.nota) from nota n;


select a.nome, c.nome, avg(n.nota) as media, avg(n.nota) - (select avg(n.nota) from nota n) as diferenca from nota n
join resposta r on r.id = n.resposta_id
join exercicio e on e.id = r.exercicio_id
join secao s on s.id = e.secao_id
join curso c on c.id = s.curso_id
join aluno a on a.id = r.aluno_id
group by c.nome, a.nome;


select a.nome from aluno a;

select count(r.id) from resposta r;

select a.nome, (select count(r.id) from resposta r where a.id = r.aluno_id) as respostas from aluno a;

select a.nome, (select count(m.id) from matricula m where a.id = m.aluno_id) as matriculas from aluno a;


select a.nome, c.nome, avg(n.nota) as media, avg(n.nota) - (select avg(n.nota) from nota n) as diferenca from nota n
join resposta r on r.id = n.resposta_id
join exercicio e on e.id = r.exercicio_id
join secao s on s.id = e.secao_id
join curso c on c.id = s.curso_id
join matricula m on m.curso_id = c.id
join aluno a on a.id = m.aluno_id
where m.data < (select sysdate - interval '6' month from dual) 
group by c.nome, a.nome;



