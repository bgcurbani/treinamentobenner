select a.nome from aluno a 
order by a.nome;

select rownum from (select a.nome from aluno a order by a.nome);

select rownum, nome from (select a.nome from aluno a order by a.nome);

select rownum, nome from (select a.nome from aluno a order by a.nome)
where rownum <= 5;

select * from (select rownum r, nome from (select a.nome from aluno a order by a.nome))
where r > 5;

select * from (select rownum r, nome from (select a.nome from aluno a order by a.nome)
where rownum <= 10)
where r > 5;

select rownum, nome from (select a.nome from aluno a order by a.nome)
where rownum <= 2;

select rownum, nome, email from (select a.nome, a.email from aluno a where email like '%.com' order by a.nome)
where rownum <= 3;