create table SegMercado (ID NUMBER(5), Descricao VARCHAR(100));

ALTER TABLE SEGMERCADO ADD CONSTRAINT SegMercado_id_pk PRIMARY KEY(ID);

CREATE TABLE Cliente (
    ID NUMBER(5),
    Razao_social VARCHAR2(100),
    CNPJ VARCHAR2(20),
    SegMercado_id Number(5),
    Data_Inclusao DATE,
    Faturameto_Previsto NUMBER(10,2),
    Categoria VARCHAR2(20));
    
    ALTER TABLE Cliente ADD CONSTRAINT Cliente_id_pk PRIMARY KEY(ID);
    
    ALTER TABLE Cliente ADD CONSTRAINT Cliente_SegMercado_fk FOREIGN KEY(SegMercado_id) REFERENCES Segmercado(id);
    
    
    