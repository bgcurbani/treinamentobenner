DECLARE
    v_id segmercado.id%type := 2;
    v_descricao segmercado.descricao%type := 'atacado';
    
BEGIN
    INSERT INTO segmercado values(v_id, upper(v_descricao));
    commit;
END;

select * from segmercado;




DECLARE
    v_id segmercado.id%type := 1;
    v_descricao segmercado.descricao%type := 'varejista';
    
BEGIN
   update segmercado set descricao = upper(v_descricao) where id = v_id;
   
   v_id := 2;
   v_descricao := 'atacadista';
   
   update segmercado set descricao = upper(v_descricao) where id = v_id;
    commit;
END;


DECLARE
    v_id segmercado.id%type := 3;
    v_descricao segmercado.descricao%type := 'esportivo';
    
BEGIN
    INSERT INTO segmercado values(v_id, upper(v_descricao));
    commit;
END;


select * from segmercado;


DECLARE
    v_id segmercado.id%type := 3;   
BEGIN
    DELETE FROM SEGMERCADO WHERE ID = v_id;
    commit;
END;














