CREATE PROCEDURE incluir_segmercado
    (p_id IN NUMBER,
    p_descricao IN VARCHAR2)
IS    
BEGIN
    INSERT INTO segmercado 
        values(p_id, upper(p_descricao));
    commit;
END;



EXECUTE  incluir_segmercado(3, 'Farmaceutico');


BEGIN
    incluir_segmercado(4, 'Industrial');
    commit;
END;



select * from segmercado;


CREATE OR REPLACE PROCEDURE incluir_segmercado
    (p_id IN segmercado.id%type,
    p_descricao IN segmercado.descricao%type)
IS    
BEGIN
    INSERT INTO segmercado 
        values(p_id, upper(p_descricao));
    commit;
END;














