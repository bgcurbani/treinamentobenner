CREATE OR REPLACE FUNCTION obter_descricao_segmercado
    (p_id IN segmercado.id%type)
        RETURN segmercado.descricao%type
IS
    v_descricao segmercado.descricao%type;
BEGIN
    SELECT descricao INTO v_descricao 
        FROM segmercado
        WHERE ID = p_id;
    RETURN v_descricao;
END;



VARIABLE g_descricao varchar2(100);
EXECUTE :g_descricao := obter_descricao_segmercado(1);
PRINT g_descricao;



SET SERVEROUTPUT ON
DECLARE
    v_descricao segmercado.descricao%type;
BEGIN
    v_descricao := obter_descricao_segmercado(2);
    dbms_output.put_line('Descricao:' ||v_descricao);
END;
        
        
        
CREATE OR REPLACE PROCEDURE INCLUIR_CLIENTE (
    p_id in cliente.id%type,
    p_razao_social in cliente.razao_social%type,
    p_CNPJ cliente.CNPJ%type,
    p_segmercado_id in cliente.segmercado_id%type,
    p_faturamento_previsto in cliente.faturamento_previsto%type)
IS
    v_categoria cliente.categoria%type;
BEGIN
    IF p_faturamento_previsto < 10000 THEN
        v_categoria := 'PEQUENO';
    ELSE IF p_faturamento_previsto < 50000 THEN
        v_categoria := 'MEDIO';
    ELSE IF p_faturamento_previsto < 100000 THEN
        v_categoria := 'MEDIO GRANDE';
    ELSE
        v_categoria := 'GRANDE';
    END IF;
    END IF;
    END IF;

    INSERT INTO cliente
        VALUES (p_id, 
                UPPER(p_razao_social), 
                p_CNPJ, 
                p_segmercado_id,
                SYSDATE,
                p_faturamento_previsto,
                v_categoria);
    COMMIT;
END; 




EXECUTE INCLUIR_CLIENTE (1, 'SUPERMERCADO XYZ', '12345', NULL, 150000);

SELECT * FROM CLIENTE;


CREATE OR REPLACE FUNCTION categoria_cliente (
    p_faturamento_previsto in cliente.faturamento_previsto%type)
    return cliente.categoria%type
IS
BEGIN

    IF p_faturamento_previsto < 10000 THEN
        RETURN 'PEQUENO';
    ELSE IF p_faturamento_previsto < 50000 THEN
            RETURN 'MEDIO';
        ELSE IF p_faturamento_previsto < 100000 THEN
                RETURN 'MEDIO GRANDE';
            ELSE
                RETURN 'GRANDE';
            END IF;
        END IF;
    END IF;
    
END;



EXECUTE INCLUIR_CLIENTE (2, 'SUPERMERCADO JFK', '67890', NULL, 90000);


SELECT * FROM CLIENTE







