select * from cliente;

create or replace procedure ATUALIZAR_CLI_SEG_MERCADO (
    p_id cliente.id%type,
    p_segmercado_id in cliente.segmercado_id%type)
is
begin
    update cliente
        set segmercado_id = p_segmercado_id
        where id = p_id;
    commit;
end;    


DECLARE
    v_segmercado_id cliente.segmercado_id%type := 1;
    v_i number(3);
BEGIN
    v_i := 1;
LOOP
    atualizar_cli_seg_mercado(v_i, v_segmercado_id);
    v_i := v_i + 1;
    EXIT WHEN v_i > 3;
END LOOP;
END;

select * from cliente;

DECLARE
    v_segmercado_id cliente.segmercado_id%type := 2;
BEGIN
    FOR i in 1..3 LOOP
        atualizar_cli_seg_mercado(i, v_segmercado_id);
    END LOOP;
    COMMIT;
END;


select * from cliente;


DECLARE
    v_id              NUMBER;
    v_segmercado_id   NUMBER;
BEGIN
    v_id := 1;
    v_segmercado_id := 2;
    atualizar_cli_seg_mercado(p_id => v_id, p_segmercado_id => v_segmercado_id);
--rollback; 
END;

select * from cliente where id = 1;
