﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ByteBank.Service.Cartao
{
    public class CartaoServiceTeste : ICartaoService
    {
        public string ObterCartaoDeCreditoDeDestaque()
        {
            return "ByteBank Gold Platinum Extra";
        }

        public string ObterCartaoDeDebitoDeDestaque()
        {
            return "ByteBank Estudante Sem Taxas";
        }
    }
}
